/**
 * Website start here
 */
$(window).load(function() {


    if ($(window).width() > 992) {


    }

});
$(window).resize(function() {



});
$(document).ready(function($) {
    $('.datetimepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });

    
    $(".menu").click(function() {
        
        if ($("body").hasClass("show")) {
            $("body").removeClass("show");
        } else {
            $("body").addClass("show");
           
        }
    });


});
$(document).click(function() {
    $(".a").removeClass("active");
    $(".a").removeClass("show");
    $("#a").removeClass("normal");
    $(".dropdown-check").removeClass("open");
});
$('.list-cata, .btn-menu, #link-search,.frm-search,.frm-search-1, .layer-menu, .btn-action, .dropdown-check .dropdown-menu').click(function(event) {
    event.stopPropagation();
});

(function($) {
    $.fn.menumaker = function(options) {
        var cssmenu = $(this),
            settings = $.extend({
                format: "dropdown",
                sticky: false
            }, options);
        return this.each(function() {
            $(this).find(".button").on('click', function() {
                $(this).toggleClass('menu-opened');
                var mainmenu = $(this).next('ul');
                if (mainmenu.hasClass('open')) {
                    mainmenu.slideToggle().removeClass('open');
                } else {
                    mainmenu.slideToggle().addClass('open');
                    if (settings.format === "dropdown") {
                        mainmenu.find('ul').show();
                    }
                }
            });

            cssmenu.find('li ul').parent().addClass('has-sub');
            multiTg = function() {
                cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
                cssmenu.find('.submenu-button').on('click', function() {
                    $(this).toggleClass('submenu-opened');
                    if ($(this).siblings('ul').hasClass('open')) {
                        $(this).siblings('ul').removeClass('open').slideToggle();
                    } else {
                        $(this).siblings('ul').addClass('open').slideToggle();
                    }
                });
            };
            if (settings.format === 'multitoggle') multiTg();
            else cssmenu.addClass('dropdown');
            if (settings.sticky === true) cssmenu.css('position', 'fixed');
            resizeFix = function() {
                var mediasize = 1000;
                if ($(window).width() > mediasize) {
                    cssmenu.find('ul').show();
                }
                if ($(window).width() <= mediasize) {
                    cssmenu.find('ul').hide().removeClass('open');
                }
            };
            resizeFix();
            return $(window).on('resize', resizeFix);
        });
    };
})(jQuery);