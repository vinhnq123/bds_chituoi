/**
 * Website start here
 */
$(window).load(function() {


    if ($(window).width() > 992) {

        $('.ui.sticky').sticky();
        $('.ui.sticky.one')
            .sticky({
                context: '.scrollOne'
            });
        $('.ui.sticky.two')
            .sticky({
                context: '.scrollTwo'
            });
    }

});
$(window).resize(function() {
    if ($(window).width() > 992) {

        $('.ui.sticky').sticky();
        $('.ui.sticky.one')
            .sticky({
                context: '.scrollOne'
            });
        $('.ui.sticky.two')
            .sticky({
                context: '.scrollTwo'
            });
    }
    $('.scrollto').click(function() {
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
        return false;
    });

    $('.dropdown-toggle').dropdown()
    if ($(window).width() < 992) {
        var nav = document.querySelector(".header");
        new Headroom(nav, {
            tolerance: {
                down: 2,
                up: 5
            },
            offset: 100,
            classes: {
                initial: "slide",
                pinned: "slide--reset",
                unpinned: "slide--up"
            }
        }).init();
    }

    $(".img-full").each(function() {
        var m = $(this).find("figcaption");
        var h = m.height();
        m.css("margin-top", -h / 2);

    });
    if ($(window).width() < 768) {
        $(".head-title").each(function() {
            var m = $(this).find("h2").width();
            var w = $(window).width();
            $(this).find(".swiper-container").css("width", w - m - 55);

        });
    }



    $(".btn-search").click(function() {
        if ($(".frm-search-mid").hasClass("active")) {
            $(".frm-search-mid").removeClass("active");
        } else {
            $(".frm-search-mid").addClass("active");
            $(".frm-search-mid input").focus();
        }
    });




    if ($(window).width() < 992) {
        $(".button").click(function() {
            if ($("body").hasClass("show")) {
                $("body").removeClass("show");
            } else {
                $("body").addClass("show");

            }
        });
    }

    var swiper = new Swiper('.swiper-container', {
        slidesPerView: "auto",
        spaceBetween: 0,
        freeMode: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });


});
$(document).ready(function($) {
    $('#datetimepicker1').datetimepicker({
        format: 'DD/MM/YYYY'
    });

    if ($('.scroll-popup').length > 0) {
        $('.scroll-popup').mCustomScrollbar({
            scrollInertia: 500
        });
    }


    $('.tab-style li a').on('shown.bs.tab', function(e) {

        if ($(window).width() > 992) {

            $('.ui.sticky').sticky();
            $('.ui.sticky.one')
                .sticky({
                    context: '.scrollOne'
                });
            $('.ui.sticky.two')
                .sticky({
                    context: '.scrollTwo'
                });
        }
    })

    $('.scrollto').click(function() {
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
        return false;
    });

    $('.dropdown-toggle').dropdown()
    if ($(window).width() < 992) {
        var nav = document.querySelector(".header");
        new Headroom(nav, {
            tolerance: {
                down: 2,
                up: 5
            },
            offset: 100,
            classes: {
                initial: "slide",
                pinned: "slide--reset",
                unpinned: "slide--up"
            }
        }).init();
    }

    $(".img-full").each(function() {
        var m = $(this).find("figcaption");
        var h = m.height();
        m.css("margin-top", -h / 2);

    });
    if ($(window).width() < 768) {
        $(".head-title").each(function() {
            var m = $(this).find("h2").width();
            var w = $(window).width();
            $(this).find(".swiper-container").css("width", w - m - 55);

        });
    }

    $(".btn-search").click(function() {
        if ($(".frm-search-mid").hasClass("active")) {
            $(".frm-search-mid").removeClass("active");
        } else {
            $(".frm-search-mid").addClass("active");
            $(".frm-search-mid input").focus();

        }
    });
    if ($(window).width() < 992) {

        $(".button").click(function() {
            if ($("body").hasClass("show")) {
                $("body").removeClass("show");
            } else {
                $("body").addClass("show");

            }
        });
    }

    setTimeout(function() {

        $('.loading').fadeOut(100, function() {
            $(this).remove();
        });
    }, 500);
    if ($(".btn-top").length > 0) {
        $(window).scroll(function() {
            var e = $(window).scrollTop();
            if (e > 150) {
                $(".btn-top").show()
            } else {
                $(".btn-top").hide()
            }
        });
        $(".btn-top").click(function() {
            $('body,html').animate({
                scrollTop: 0
            })
        });
    }
    if ($(".btn-searchsg").length > 0) {
        $(".btn-searchsg").click(function() {
            if ($(".frm-search-1").hasClass("active")) {
                $(".frm-search-1").removeClass("active");
            } else {
                $(".frm-search-1").addClass("active");
                return false;
            }
        });
    }



    $('.slick-result-1').slick({
        arrows: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 2000,
        fade: true
    });
    $('.slider-commercial').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        responsive: [{
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider-for',


        focusOnSelect: true,
        responsive: [{
                breakpoint: 767,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    $('.slide-one').slick({
        arrows: false,
        dots: true
    });
    if ($(window).width() > 992) {
        $('.slide-second').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false,
            dots: true
        });
    }
    $('.slider-magazine').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: true,
        dots: true,
        responsive: [{
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    if ($(window).width() > 992) {
        if ($('.scroll').length > 0) {
            $('.scroll').mCustomScrollbar({
                scrollInertia: 500
            });
        }
    }
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: "auto",
        spaceBetween: 0,
        freeMode: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });
    $("#imageUpload").change(function() {
        readURL(this);
    });
    $('input[type="checkbox"]').change(function(e) {

        var checked = $(this).prop("checked"),
            container = $(this).parent(),
            siblings = container.siblings();

        container.find('input[type="checkbox"]').prop({
            indeterminate: false,
            checked: checked
        });

        function checkSiblings(el) {

            var parent = el.parent().parent(),
                all = true;

            el.siblings().each(function() {
                return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
            });

            if (all && checked) {

                parent.children('input[type="checkbox"]').prop({
                    indeterminate: false,
                    checked: checked
                });

                checkSiblings(parent);

            } else if (all && !checked) {

                parent.children('input[type="checkbox"]').prop("checked", checked);
                parent.children('input[type="checkbox"]').prop("indeterminate", (parent.find('input[type="checkbox"]:checked').length > 0));
                checkSiblings(parent);

            } else {

                el.parents("li").children('input[type="checkbox"]').prop({
                    indeterminate: true,
                    checked: false
                });

            }

        }

        checkSiblings(container);
    });


});
$(document).click(function() {
    $(".a").removeClass("active");
    $(".a").removeClass("show");
    $("#a").removeClass("normal");
    $(".dropdown-check").removeClass("open");
});
$('.list-cata, .btn-menu, #link-search,.frm-search,.frm-search-1, .layer-menu, .btn-action, .dropdown-check .dropdown-menu').click(function(event) {
    event.stopPropagation();
});

(function($) {
    $.fn.menumaker = function(options) {
        var cssmenu = $(this),
            settings = $.extend({
                format: "dropdown",
                sticky: false
            }, options);
        return this.each(function() {
            $(this).find(".button").on('click', function() {
                $(this).toggleClass('menu-opened');
                var mainmenu = $(this).next('ul');
                if (mainmenu.hasClass('open')) {
                    mainmenu.slideToggle().removeClass('open');
                } else {
                    mainmenu.slideToggle().addClass('open');
                    if (settings.format === "dropdown") {
                        mainmenu.find('ul').show();
                    }
                }
            });

            cssmenu.find('li ul').parent().addClass('has-sub');
            multiTg = function() {
                cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
                cssmenu.find('.submenu-button').on('click', function() {
                    $(this).toggleClass('submenu-opened');
                    if ($(this).siblings('ul').hasClass('open')) {
                        $(this).siblings('ul').removeClass('open').slideToggle();
                    } else {
                        $(this).siblings('ul').addClass('open').slideToggle();
                    }
                });
            };
            if (settings.format === 'multitoggle') multiTg();
            else cssmenu.addClass('dropdown');
            if (settings.sticky === true) cssmenu.css('position', 'fixed');
            resizeFix = function() {
                var mediasize = 1000;
                if ($(window).width() > mediasize) {
                    cssmenu.find('ul').show();
                }
                if ($(window).width() <= mediasize) {
                    cssmenu.find('ul').hide().removeClass('open');
                }
            };
            resizeFix();
            return $(window).on('resize', resizeFix);
        });
    };
})(jQuery);

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}